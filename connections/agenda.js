const Agenda = require('agenda');
const {
    MONGO_HOST,
    MONGO_PORT,
    MONGO_USER,
    MONGO_PASSWORD,
    MONGO_DB,
} = process.env;

const mongoConnectionString = `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_DB}`;

const agenda = new Agenda({ db: { address: mongoConnectionString } });

agenda.on('ready', () => {
    console.log(`MongoDB connection established. Database: ${MONGO_DB}`);
});

agenda.on('error', (err) => {
    console.log('Agenda error occurred');
    console.log(err);
});

module.exports = agenda;
