module.exports = agenda => ({

    createJob: async(croneString, jobName, locationId, data = {}) => {
        try {
            const jobs = await agenda.jobs(
                { location_id: locationId },
                {},
                1
            );
            if (jobs.length) {
                await jobs[0].remove();
            }
            const job = await agenda.create(jobName, data).repeatEvery(croneString);
            job.attrs.location_id = locationId;
            await job.save();
        }
        catch(err) {
            console.log(err);
        }
    },

    deleteJob: async(locationId) => {
        try {
            const jobs = await agenda.jobs(
                { location_id: locationId },
                {},
                1
            );
            await jobs[0].remove();
        }
        catch(err) {
            console.log(err);
        }
    },

    enableJob: async(locationId) => {
        try {
            const jobs = await agenda.jobs(
                { location_id: locationId },
                {},
                1
            );
            const [job] = jobs;
            await job.enable();
            await job.save();
        }
        catch(err) {
            console.log(err);
        }
    },

    disableJob: async(locationId) => {
        try {
            const jobs = await agenda.jobs(
                { location_id: locationId },
                {},
                1,
            );
            const [job] = jobs;
            await job.disable();
            await job.save();
        }
        catch(err) {
            console.log(err);
        }
    },

})
