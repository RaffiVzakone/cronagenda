if (process.env.NODE_ENV === 'development') {
    require('dotenv').config();
}
//connect to mongoDB
const agenda = require('./connections/agenda');
//define sync behaviour
require('./sync/blaze')(agenda);
//get controller factory
const locationJobControllerFactory = require('./controllers/locationJob');

(async () => {
    try {
        await agenda.start();
        const locationJobController = locationJobControllerFactory(agenda);
    }
    catch(err) {
        console.log(err)
    }
})();

//graceful shutdown
async function graceful() {
    await agenda.stop();
    process.exit(0);
}

process.on("SIGTERM", graceful);
process.on("SIGINT", graceful);
