## Run on production
1 Run `npm run mongoSetup`

2 Run `npm run start`

###### Note. You must supply env variables to both processes. Necessary env variables are defined in `.env.example` file.

*In MongoDB collection name `agendaJobs` must be vacant*

## Run on development

1 Create `.env` file in the root of the project and fill in env variables that are defined in `.env.example` file

2 Run `npm run mongoSetupDev`

3 Run `npm run dev`

*In MongoDB collection name `agendaJobs` must be vacant*
