if (process.env.NODE_ENV === 'development') {
    require('dotenv').config();
}
const { MongoClient } = require("mongodb");
const {
    MONGO_HOST,
    MONGO_PORT,
    MONGO_USER,
    MONGO_PASSWORD,
    MONGO_DB,
} = process.env;

const mongoConnectionString = `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_DB}`;

(async () => {
    try {
        const client = new MongoClient(mongoConnectionString);
        await client.connect();
        const database = client.db(MONGO_DB);
        const collection = database.collection("agendaJobs");
        await collection.createIndex({ location_id: 1 }, { unique: true });
        process.exit(0);
    }
    catch(err) {
        console.log(err);
    }
})();
