const { blaze } = require('../constants/jobNames');

module.exports = agenda => {
    agenda.define(blaze, async (job) => {
        const { location_id, user_id } = job.attrs.data;
        console.log('blaze sync start');
        //blaze sync business logic goes here
    });
}
